## Entwicklungsumgebung in Windows System

### Einstellen der Umgebungsvariablen
```
    setx TEMP e:\AppData\Local\Temp
    setx TMP e:\AppData\Local\Temp
     setx CARGO_HOME  e:\CARGO_HOME
     setx RUSTUP_HOME  e:\RUSTUP_HOME
     setx GOPATH e:\gopath
     
     
```

### Folgende Programme müssen installiert sein

#### Antivirus Software
+ [bitdefender](https://www.bitdefender.de/solutions/free.html)

#### Browser
+ [Firefox](https://www.mozilla.org/de/firefox/new/)
+ [Chrome](https://www.google.com/chrome/)
+ [Opera](https://www.opera.com/de)
+ [Edge](https://www.microsoft.com/de-de/edge)

#### Versionierungssysteme
+ [Git](https://git-scm.com/downloads)

#### Build Tools
+ [Visual Cpp Build Tools nur C++[x64]](https://visualstudio.microsoft.com/de/visual-cpp-build-tools/)
+ Msys2 [x64](https://www.msys2.org/) [x86](https://www.msys2.org/)
+ [MSYS2 Packages](https://packages.msys2.org/updates)

#### MYSys2 Packages sind zu installieren
```
    pacman -S mingw-w64-x86_64-toolchain base-devel mingw-w64-x86_64-pkg-config mingw-w64-x86_64-curl mingw-w64-x86_64-cmake
```     
#### Programmiersprachen
+ [Go](https://golang.org/dl/)
+ [Rust](https://www.rust-lang.org/tools/install)
+ [Python](https://www.python.org/downloads/)
+ [Node.js](https://nodejs.org/en/download/)

#### IDEs
+ [Visual Studio Code](https://code.visualstudio.com/download)
+ [IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows)
+ [CLion](https://www.jetbrains.com/clion/download/#section=windows)

#### Datenbanken
+ [PostgreSQL](https://www.postgresql.org/download/windows/)











