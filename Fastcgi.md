## Fastcgi mit C++ und nginx
### Was ist Fastcgi?
FastCGI ist ein Protokoll, das die Kommunikation zwischen einem Webserver und einer Anwendung ermöglicht. Es ist eine Erweiterung des Common Gateway Interface (CGI). FastCGI ist eine Schnittstelle zwischen einem Webserver und einer Anwendung, die die Kommunikation zwischen den beiden ermöglicht. Es ist eine Erweiterung des Common Gateway Interface (CGI). FastCGI ist eine Schnittstelle zwischen einem Webserver und einer Anwendung, die die Kommunikation zwischen den beiden ermöglicht.

### Wie installiere ich Fastcgi auf Ubuntu?
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo apt-get install libfcgi-dev
```
* Installieren Sie nginx mit dem folgenden Befehl:
```
sudo apt-get install nginx
```
* Installieren Sie build-essential mit dem folgenden Befehl:
```
sudo apt-get install build-essential
```
* Installieren Sie CMake mit dem folgenden Befehl:
```
sudo apt-get install cmake
```
* Installieren Sie gdb mit dem folgenden Befehl:
```
sudo apt-get install gdb
```
## Was ist spawn-fcgi?
Spawn-fcgi ist

## Installieren Sie spawn-fcgi mit dem folgenden Befehl:
```
sudo apt install spawn-fcgi
```
## Systemd-Service für spawn-fcgi erstellen
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo nano /etc/systemd/system/start.service
```
* Fügen Sie die folgenden Zeilen in die Datei ein:
```
[Unit]
Description=Mein Programm FastCGI
After=syslog.target network.target

[Service]
Type=forking
User=root
ExecStart=/usr/bin/spawn-fcgi -p 8087 /home/thorsten/websiteC++/cmake-build-debug/websiteC__

[Install]
WantedBy=multi-user.target
```
* Speichern Sie die Datei und schließen Sie den Editor.
* Starten Sie den Service mit dem folgenden Befehl:
```
sudo systemctl start start.service
```
* Überprüfen Sie den Status des Dienstes mit dem folgenden Befehl:
```
sudo systemctl status start.service
```
* Aktivieren Sie den Service mit dem folgenden Befehl:
```
sudo systemctl enable start.service
```
* Überprüfen Sie den Status des Dienstes mit dem folgenden Befehl:
```
sudo systemctl status start.service
```

## nginx Konfiguration
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo nano /etc/nginx/conf.d/start.conf
```
* Fügen Sie die folgenden Zeilen in die Datei ein:
```nginx
server {
    listen 80;
    server_name test.localhost;
  
    location / {
        fastcgi_pass 127.0.0.1:8087;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

* Speichern Sie die Datei und schließen Sie den Editor.
* Überprüfen Sie die Syntax mit dem folgenden Befehl:
```
sudo nginx -t
```
* Starten Sie nginx mit dem folgenden Befehl:
```
sudo systemctl restart nginx
```
* Überprüfen Sie den Status von nginx mit dem folgenden Befehl:
```
sudo systemctl status nginx
```






