## Tomcat, GeoServer und Nginx auf Ubuntu 22.04 Installieren und Konfigurieren

Der Installationsprozess für Tomcat, GeoServer und Nginx auf Ubuntu 22.04 ist dem Prozess in meinen vorherigen Antworten sehr ähnlich. Hier sind die Schritte, um diese Anwendungen zu installieren und GeoServer so zu konfigurieren, dass er mit Nginx funktioniert:

* Installieren Sie Java 11
```
sudo apt update
sudo apt install openjdk-11-jdk
```
* Installieren Sie Tomcat 9
```
sudo apt install tomcat9 tomcat9-admin
```
* Starten Sie Tomcat 9
```
sudo systemctl start tomcat9
``` 
* Installieren Sie nginx
```
sudo apt install nginx
```
* Starten Sie nginx
```
sudo systemctl start nginx
```

* Laden Sie die neueste Version von GeoServer von der GeoServer-Website herunter und entpacken Sie sie in das Tomcat-Webapps-Verzeichnis
```
cd /var/lib/tomcat9/webapps
wget https://build.geoserver.org/geoserver/2.23.x/geoserver-2.23.x-latest-war.zip
unzip geoserver-2.23.x-latest-war.zip
rm geoserver-2.23.x-latest-war.zip
```
## Konfigurieren von GeoServer

* Öffnen Sie die Datei /var/lib/tomcat9/webapps/geoserver/WEB-INF/web.xml in einem Texteditor wie nano:
```
sudo nano /var/lib/tomcat9/webapps/geoserver/WEB-INF/web.xml
```
* Suchen Sie nach der Zeile </web-app> am Ende der Datei.
* Fügen Sie die folgenden Zeilen direkt über die </web-app>-Zeile ein:
```
<context-param>
   <param-name>GEOSERVER_CSRF_WHITELIST</param-name>
   <param-value>ahrensburg.city</param-value>
</context-param>
```
* Speichern Sie die Datei und schließen Sie den Editor.
* Starten Sie Tomcat neu:
```
sudo systemctl restart tomcat9
```

## Backup von GeoServer

* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo apt install zip
```
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo zip -r /var/lib/tomcat9/webapps/geoserver.zip /var/lib/tomcat9/webapps/geoserver
```

## Wiederherstellen von GeoServer

* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo unzip /var/lib/tomcat9/webapps/geoserver.zip -d /var/lib/tomcat9/webapps/
```
Rechte für GeoServer-Verzeichnis

* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
```
sudo chown -R tomcat9:tomcat9 /var/lib/tomcat9/webapps/geoserver
```








