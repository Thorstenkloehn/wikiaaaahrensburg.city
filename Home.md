## Willkommen bei GitHub Wikis Seite
## Was ist das hier?
Das ist ein kleines Projekt, welches ich für mich selbst erstellt habe
um meine Stadt Ahrensburg zu erkunden. Es ist ein kleines Programm, welches
eine Karte von [OpenStreetMap](https://www.openstreetmap.org) lädt
und diese dann mit verschiedenen Informationen anzeigt. Die
Informationen werden von [OpenStreetMap](https://www.openstreetmap.org) bezogen.

## Was kann ich damit machen?
Das Programm kann verschiedene Informationen anzeigen. Diese
Informationen werden von [OpenStreetMap](https://www.openstreetmap.org) bezogen.
## Voraussetzungen
* Programmsprache Go installiert auf dem System ist.
* Git installiert auf dem System ist.

## Installieren
### Git Projekt herunterladen
Der Befehl git clone  ist ein Befehl, der das Projekt herunterlädt. Hier ist das Projekt auf Github zu finden.
* git clone https://github.com/thorstenkloehn/ahrensburg.city.git  Karte

### Gehe in das Verzeichnis Karte
* cd Karte
### Kompilieren
* go build



## Starten des Programms

### Windows

* Karte.exe

### Linux

* ./Karte


