## Postges Ubuntu 23.04

### Postgres installieren

```bash
sudo apt install postgresql postgresql-contrib
```
Postgres version prüfen
```bash
psql --version
```

### Postgis installieren

```bash
sudo apt install postgis postgresql-15-postgis-3
```

### Postgres starten

```bash
sudo service postgresql start
```

### Postgres stoppen

```bash
sudo service postgresql stop
```

### Postgres neu starten

```bash
sudo service postgresql restart
```

### Postgres Status prüfen

```bash
sudo service postgresql status
```

### Postgres Datenbank erstellen

```bash
sudo -u postgres createdb ahrensburg
```

### Postgres Datenbank Postgis aktivieren

```bash 
sudo -u postgres psql -d ahrensburg -c "CREATE EXTENSION postgis;"
```

### Postgres Datenbank hstore aktivieren

```bash
sudo -u postgres psql -d ahrensburg -c "CREATE EXTENSION hstore;"
```

### Postgres Passwort ändern

```bash
sudo -u postgres psql
\password postgres
```

## Osm2pgsql

### Osm2pgsql installieren

```bash
sudo apt install osm2pgsql
```
## Osm Daten herunterladen

```bash
mkdir ~/data
cd ~/data
wget https://download.geofabrik.de/europe/germany/schleswig-holstein-latest.osm.pbf
```

### Osm Daten importieren

```bash
cd ~/data
osm2pgsql -d ahrensburg -U postgres -H localhost -W -S /usr/share/osm2pgsql/default.style -C 2000 schleswig-holstein-latest.osm.pbf
``` 







    








