## Kleine Projekte oder Prototypen:
* Python (mit Django oder Flask)
* node.js (mit Express)

## Mittlere bis große Projekte:
* Java (mit Spring)
* ASP.NET (mit C#)
* Go

## Sehr große, leistungsintensive Projekte:
* C++ (mit drogon)
* Rust 

* https://www.techempower.com/benchmarks/#section=data-r21 

