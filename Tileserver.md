## Quellangabe
Dieser Text basiert auf dem Artikel [Ubuntu 22.4](https://switch2osm.org/serving-tiles/manually-building-a-tile-server-ubuntu-22-04-lts/)  aus der [OpenStreetMap](https://switch2osm.org/) and contributor und steht unter der Lizenz [CC BY-SA](http://creativecommons.org/licenses/by-sa/2.0/)
und Maschinelle Übersetzung

## Inhalt

Auf dieser Seite wird beschrieben, wie Sie alle erforderliche Software installieren, einrichten und konfigurieren, um Ihren eigenen Kachelserver zu betreiben. Diese Schritt-für-Schritt-Anleitung wurde für Ubuntu Linux 22.04 (Jammy Jellyfish) geschrieben und im April 2022 getestet.

## Software Installation

Der OSM-Tileserver-Stack ist eine Sammlung von Programmen und Bibliotheken, die zusammenarbeiten, um einen Tileserver zu erstellen. Wie so oft bei OpenStreetMap gibt es viele Wege, um dieses Ziel zu erreichen, und für fast alle Komponenten gibt es Alternativen, die verschiedene spezifische Vor- und Nachteile haben. Dieses Tutorial beschreibt die Standardversion, die derjenigen ähnelt, die auf den Hauptkachelservern von OpenStreetMap.org verwendet wird.

Es besteht aus 5 Hauptkomponenten: mod_tile, renderd, mapnik, osm2pgsql und einer postgresql/postgis-Datenbank. Mod_tile ist ein Apache-Modul, das zwischengespeicherte Kacheln bereitstellt und entscheidet, welche Kacheln neu gerendert werden müssen - entweder weil sie noch nicht zwischengespeichert wurden oder weil sie veraltet sind. Renderd bietet ein Prioritätswarteschlangensystem für verschiedene Arten von Anfragen, um die Last von Renderanfragen zu verwalten und zu glätten. Mapnik ist die Softwarebibliothek, die das eigentliche Rendern durchführt und von renderd verwendet wird.

Dank der Arbeit der Debian- und Ubuntu-Betreuer, die neuesten Versionen dieser Pakete in Ubuntu 22.04 zu integrieren, sind diese Anweisungen etwas kürzer als in früheren Versionen.

Diese Anweisungen wurden geschrieben und auf einem neu installierten Ubuntu 22.04-Server getestet. Wenn Sie bereits andere Versionen einer Software installiert haben (vielleicht haben Sie ein Upgrade von einer früheren Version durchgeführt oder einige PPAs zum Laden eingerichtet), müssen Sie möglicherweise einige Anpassungen vornehmen.

Um diese Komponenten zu erstellen, müssen zunächst verschiedene Abhängigkeiten installiert werden.

Diese Anleitung geht davon aus, dass Sie alles von einem Nicht-Root-Benutzer über „sudo“ ausführen. Versuchen Sie nicht, alles unten als root auszuführen; es wird nicht funktionieren.


```
sudo -i 
adduser thorsten 
usermod -aG sudo thorsten
exit
ssh thorsten@ahrensburg.city
sudo apt update
sudo apt upgrade
sudo apt install screen locate libapache2-mod-tile renderd git tar unzip wget bzip2 apache2 lua5.1 mapnik-utils python3-mapnik python3-psycopg2 python3-yaml gdal-bin npm fonts-noto-cjk fonts-noto-hinted fonts-noto-unhinted fonts-unifont fonts-hanazono postgresql postgresql-contrib postgis postgresql-14-postgis-3 postgresql-14-postgis-3-scripts osm2pgsql net-tools
```
Zu diesem Zeitpunkt wurden einige neue Konten hinzugefügt. Sie können sie mit „tail /etc/passwd“ sehen. „postgres“ wird zum Verwalten der Datenbanken verwendet, die wir zum Speichern von Daten zum Rendern verwenden. „_renderd“ wird für den renderd-Daemon verwendet, und wir müssen sicherstellen, dass viele der folgenden Befehle als dieser Benutzer ausgeführt werden.
Jetzt müssen Sie eine Postgis-Datenbank erstellen. Die Standardeinstellungen verschiedener Programme gehen davon aus, dass die Datenbank gis heißt, und wir werden in diesem Tutorial dieselbe Konvention verwenden, obwohl dies nicht notwendig ist. Beachten Sie, dass „_renderd“ unten mit dem Benutzer übereinstimmt, von dem der renderd-Daemon ausgeführt wird.
```
sudo -u postgres -i
createuser thorsten
createdb -E UTF8 -O thorsten gis
```

Während Sie noch als Benutzer „postgres“ arbeiten, richten Sie PostGIS in der PostgreSQL-Datenbank ein:

```
psql

```

(dadurch gelangen Sie zu einer „postgres=#“-Eingabeaufforderung)

```
\c gis
```

(Es wird geantwortet: „Sie sind jetzt mit der Datenbank „gis“ als Benutzer „postgres“ verbunden“.)

```
CREATE EXTENSION postgis;

```
(es wird CREATE EXTENSION antworten)

```
CREATE EXTENSION hstore;


```
(es wird CREATE EXTENSION antworten)

```
ALTER TABLE geometry_columns OWNER TO thorsten;
```
(es wird ALTER TABLE antworten)
``` 

ALTER TABLE spatial_ref_sys OWNER TO thorsten;


```
(es wird ALTER TABLE antworten)

```
\q

```
(Es wird psql beenden und zu einer normalen Linux-Eingabeaufforderung zurückkehren.)

```
exit
```

(um wieder der Benutzer zu sein, der wir waren, bevor wir oben „sudo -u postgres -i“ gemacht haben)

## Mapnik

Mapnik wurde oben installiert. Wir überprüfen, ob es korrekt installiert wurde, indem wir Folgendes tun:

```
python3
>>> import mapnik
>>>

```
Wenn Python mit dem zweiten Chevron-Prompt »> und ohne Fehler antwortet, wurde die Mapnik-Bibliothek von Python gefunden. Herzliche Glückwünsche! Sie können Python mit diesem Befehl verlassen:

```
>>> quit()
```

## Stylesheet-Konfiguration

Nachdem die gesamte erforderliche Software installiert ist, müssen Sie ein Stylesheet herunterladen und konfigurieren.

Der Stil, den wir hier verwenden, ist derjenige, der von der „Standard“-Karte auf der Website openstreetmap.org verwendet wird. Es wurde ausgewählt, weil es gut dokumentiert ist und überall auf der Welt funktionieren sollte (einschließlich an Orten mit nicht-lateinischen Ortsnamen). Es gibt jedoch ein paar Nachteile - es ist ein Kompromiss, der darauf ausgelegt ist, global zu funktionieren, und es ist ziemlich kompliziert zu verstehen und zu ändern, falls Sie dies tun müssen.

Die Heimat von „OpenStreetMap Carto“ im Web ist https://github.com/gravitystorm/openstreetmap-carto/ und es gibt eine eigene Installationsanleitung unter https://github.com/gravitystorm/openstreetmap-carto/blob/master /INSTALL.md , obwohl wir hier alles behandeln, was getan werden muss.

Hier gehen wir davon aus, dass wir die Stylesheet-Details in einem Verzeichnis unterhalb von „src“ unterhalb des Home-Verzeichnisses des von Ihnen verwendeten Nicht-Root-Benutzerkontos speichern; Wir werden den Zugriff so ändern, dass der Benutzer „_renderd“ unten darauf zugreifen kann.
```
mkdir ~/src
cd ~/src
git clone https://github.com/gravitystorm/openstreetmap-carto
cd openstreetmap-carto

```

Als nächstes installieren wir eine passende Version des „carto“-Compilers.

```
sudo npm install -g carto
carto -v

```

Das sollte mit einer Zahl antworten, die mindestens so hoch ist wie:
```
1.2.0
```
Dann konvertieren wir das Carto-Projekt in etwas, das Mapnik verstehen kann:

````
carto project.mml > mapnik.xml
````
Sie haben jetzt ein XML-Stylesheet von Mapnik unter /home/youruseraccount/src/openstreetmap-carto/mapnik.xml .

## Lade Daten
```
mkdir ~/data
cd ~/data
wget https://download.geofabrik.de/europe/germany/schleswig-holstein-latest.osm.pbf

```

Als nächstes müssen wir sicherstellen, dass der Benutzer „_renderd“ auf das Stylesheet zugreifen kann. Dazu benötigt es Zugriff auf den Ort, an dem Sie es heruntergeladen haben, und standardmäßig hat es keinen Zugriff auf Ihr Home-Verzeichnis. Wenn es dann in „src“ unter Ihrem Benutzerkonto steht

```
chmod o+rx ~

```
wird funktionieren. Wenn Sie dies nicht möchten, können Sie es verschieben und Verweise auf die Dateispeicherorte in nachfolgenden Befehlen ändern.

Der folgende Befehl fügt die zuvor heruntergeladenen OpenStreetMap-Daten in die Datenbank ein. Dieser Schritt ist sehr I/O-intensiv auf der Festplatte; Das Importieren des gesamten Planeten kann je nach Hardware viele Stunden, Tage oder Wochen dauern. Bei kleineren Extrakten ist die Importzeit entsprechend viel kürzer, und Sie müssen möglicherweise mit verschiedenen -C-Werten experimentieren, um sie in den verfügbaren Speicher Ihres Computers zu integrieren. Beachten Sie, dass für diesen Vorgang der Benutzer „thorsten“ verwendet wird.


```

sudo -u thorsten osm2pgsql -d gis --create --slim  -G --hstore --tag-transform-script ~/src/openst
reetmap-carto/openstreetmap-carto.lua -C 2500 --number-processes 1 -S ~/src/openstreetmap-carto/openstreetmap-carto.styl
e ~/data/schleswig-holstein-latest.osm.pbf

```
Das letzte Argument ist die zu ladende Datendatei.

Dieser Befehl wird mit etwas wie „osm2pgsql hat insgesamt 163 Sekunden (2 Minuten 43 Sekunden) gedauert“ abgeschlossen.

## Indizes erstellen

```
cd ~/src/openstreetmap-carto/
sudo -u thorsten psql -d gis -f indexes.sql

```

Es sollte 14 Mal mit „CREATE INDEX“ antworten.

## Shapefile-Download
Obwohl die meisten Daten, die zum Erstellen der Karte verwendet werden, direkt aus der OpenStreetMap-Datendatei stammen, die Sie oben heruntergeladen haben, werden noch einige Shapefiles für Dinge wie Ländergrenzen mit niedrigem Zoom benötigt. Um diese herunterzuladen und zu indizieren, verwenden Sie dasselbe Konto wie zuvor:

```
cd ~/src/openstreetmap-carto/
mkdir data
sudo chown thorsten data
sudo -u thorsten scripts/get-external-data.py
```

Dieser Vorgang beinhaltet einen beträchtlichen Download und kann einige Zeit dauern - es wird nicht viel auf dem Bildschirm angezeigt, wenn er ausgeführt wird. Einige Daten gehen direkt in die Datenbank, andere in ein „data“-Verzeichnis unterhalb von „openstreetmap-carto“. Wenn es hier ein Problem gibt, dann sind die Daten von Natural Earth möglicherweise verschoben worden – sehen Sie sich dieses Problem und andere Probleme bei Natural Earth an, um weitere Einzelheiten zu erfahren. Wenn Sie den Download-Speicherort von Natural Earth ändern müssen, müssen Sie Ihre Kopie dieser Datei bearbeiten.
## Schriftarten

In Version v5.6.0 und höher von Carto müssen Schriftarten manuell installiert werden:
```
cd ~/src/openstreetmap-carto/
scripts/get-fonts.sh
```
Unser Testdatengebiet (Aserbaidschan) wurde ausgewählt, weil es ein kleines Gebiet war und weil einige Ortsnamen in dieser Region Namen haben, die nicht-lateinische Zeichen enthalten.

## Einrichten Ihres Webservers

### Rendern konfigurieren
Die Konfigurationsdatei für „renderd“ unter Ubuntu 22.04 ist „/etc/renderd.conf“. Bearbeiten Sie das mit einem Texteditor wie Nano:

```
sudo nano /etc/renderd.conf

```
Fügen Sie am Ende einen Abschnitt wie den folgenden hinzu:

```
[s2o]
URI=/karte/
XML=/home/thorsten/src/openstreetmap-carto/mapnik.xml
HOST=localhost
TILESIZE=256
MAXZOOM=20
```
Der Speicherort der XML-Datei „/home/accountname/src/openstreetmap-carto/mapnik.xml“ muss auf den tatsächlichen Speicherort auf Ihrem System geändert werden. Sie können „[s2o]“ und „URI=/hot/“ auch ändern, wenn Sie möchten. Wenn Sie mehr als einen Kachelsatz von einem Server rendern möchten, können Sie das tun – fügen Sie einfach einen weiteren Abschnitt wie „[s2o]“ mit einem anderen Namen hinzu, der sich auf einen anderen Kartenstil bezieht. Wenn Sie möchten, dass es sich auf eine andere Datenbank als die standardmäßige „gis“ bezieht, können Sie dies tun, aber das würde den Rahmen dieses Dokuments sprengen. Wenn Sie nur etwa 2 GB Speicher haben, sollten Sie auch „num_threads“ auf 2 reduzieren. „URI=/hot/“ wurde gewählt, damit die hier generierten Kacheln einfacher anstelle der HOT-Kachel verwendet werden können Ebene auf OpenStreetMap.org. Sie können hier etwas anderes verwenden, aber „/hot/“ ist so gut wie alles.

Als diese Anleitung zum ersten Mal geschrieben wurde, war die von Ubuntu 22.04 bereitgestellte Version von Mapnik 3.1, und die Einstellung „plugins_dir“ im „[mapnik]“-Teil der Datei war „/usr/lib/mapnik/3.1/input“. Dieses „3.1“ kann sich in Zukunft wieder ändern. Wenn beim Versuch, Kacheln wie diese zu rendern, ein Fehler auftritt:

An error occurred while loading the map layer 's2o': Could not create datasource for type: 'postgis' (no datasource plugin directories have been successfully registered)  encountered during parsing of layer 'landcover-low-zoom'
schauen Sie dann in „/usr/lib/mapnik“ nach, welche Versionsverzeichnisse es dort gibt, und schauen Sie auch in „/usr/lib/mapnik/(version)/input“, um sicherzustellen, dass dort eine Datei „postgis.input“ existiert .

### Stellen Sie sicher, dass Sie Debug-Meldungen sehen können
An dieser Stelle wäre es wirklich nützlich, die Ausgabe des Kachel-Rendering-Prozesses einschließlich aller Fehler sehen zu können. Standardmäßig ist dies bei neueren mod_tile-Versionen deaktiviert. Einschalten:

```

sudo nano /usr/lib/systemd/system/renderd.service

```


## Datei Rechte

```
sudo chown _renderd  /var/cache/renderd/tiles
sudo chown _renderd /run/renderd

sudo chmod 777 /var/cache/renderd/tiles
sudo chmod 777 /run/renderd


```
## Restart
```
sudo systemctl daemon-reload
sudo systemctl restart renderd
sudo systemctl restart apache2
```
## Apache Port ändern 3000

sudo nano /etc/apache2/ports.conf

```
LISTEN 3000
```

Restarten

```
sudo -u thorsten renderd -f -c /etc/renderd.conf

```

## Nginx Konfiguration

## Nginx Installation

``
sudo apt install nginx
``
## Nginx Konfiguration


Um Nginx als Reverse Proxy für Apache auf Port 3000 zu konfigurieren, können Sie die folgenden Schritte ausführen:
+ Stellen Sie sicher, dass Apache auf Port 3000 läuft, indem Sie die Apache-Konfigurationsdatei ports.conf bearbeiten:
```
sudo nano /etc/apache2/ports.conf
```
+ Ändern Sie Listen 80 in Listen 3000

+ Apache neu starten:
```
sudo systemctl restart apache2
```

+ Stellen Sie sicher, dass Nginx auf Port 80 läuft, indem Sie die Nginx-Konfigurationsdatei default bearbeiten:
```
sudo nano /etc/nginx/conf.d/karte.conf
```

+ Fügen Sie die folgenden Zeilen hinzu:
```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    location / {
        return 301 https://$host$request_uri;
    }
}

 server {
 listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name ahrensburg.city;
      ssl_certificate     /etc/letsencrypt/live/ahrensburg.city/fullchain.pem;
     ssl_certificate_key /etc/letsencrypt/live/ahrensburg.city/privkey.pem;
   
        location / {
       root /Server/ahrensburgkarte;
       index  index.html index.htm;
        gzip on;
}

    location /karte {
        proxy_pass http://localhost:3000/karte;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}

```
## systemctl erstellen
Der Befehl "sudo -u thorsten renderd -f -c /etc/renderd.conf" startet den "renderd"-Prozess mit dem Benutzer "thorsten" und der Konfigurationsdatei "/etc/renderd.conf". Um diesen Prozess als Systemd-Service zu starten, können Sie den folgenden Befehl ausführen:
```
sudo nano /etc/systemd/system/thorsten.service
```
+ Fügen Sie die folgenden Zeilen hinzu:
```
[Unit]
Description=OpenStreetMap rendering daemon
After=postgresql.service

[Service]
User=thorsten
ExecStart=/usr/bin/renderd -f -c /etc/renderd.conf
Restart=always

[Install]
WantedBy=multi-user.target
```
+ Starten Sie den renderd-Dienst:
```
sudo systemctl start thorsten
```
+ Überprüfen Sie den Status des Diensts:
```
sudo systemctl status thorsten
```
+ Wenn alles gut läuft, können Sie den Dienst automatisch starten, wenn der Server neu gestartet wird:
```
sudo systemctl enable thorsten
```


## Quellangabe
Dieser Text basiert auf dem Artikel [Ubuntu 22.4](https://switch2osm.org/serving-tiles/manually-building-a-tile-server-ubuntu-22-04-lts/)  aus der [OpenStreetMap](https://switch2osm.org/) and contributor und steht unter der Lizenz [CC BY-SA](http://creativecommons.org/licenses/by-sa/2.0/)
und Maschinelle Übersetzung
