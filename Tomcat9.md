## Was ist Tomcat?
Tomcat ist ein Open-Source-Webserver, der auf der Java-Plattform basiert. Es ist ein Servlet-Container, der Java Servlets und JavaServer Pages (JSP) ausführt. 
## Wie installiere ich Tomcat auf Ubuntu?
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
* Aktualisieren Sie Ihre Paketliste und Pakete, indem Sie den folgenden Befehl ausführen:
```
sudo apt update && sudo apt upgrade
```
// was ist openjdk
Das openjdk ist ein Open-Source-Implementierung der Java-Plattform. Es ist eine freie Implementierung der Java-Plattform, die von Oracle entwickelt wird.
* Installieren Sie openjdk mit dem folgenden Befehl:
```


sudo apt-get install openjdk-11-jdk
```

* Installieren Sie Tomcat mit dem folgenden Befehl:
```
sudo apt install tomcat9 tomcat9-admin 
```
* Starten Sie Tomcat mit dem folgenden Befehl:
```
sudo systemctl start tomcat9
``` 
## nginx Konfiguration
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:

