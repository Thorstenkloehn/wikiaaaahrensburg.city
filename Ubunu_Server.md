## Ubuntu Install Server
```
sudo apt-get update
sudo apt-get upgrade
sudo do-release-upgrade
sudo apt-get install git-core
sudo apt-get install  gnupg2
sudo apt install python3 python3-dev curl python-is-python3 -y
sudo apt install python3-pip -y
sudo apt-get install openjdk-11-jdk
sudo apt install tomcat9
``` 

## Installieren nginx
```
sudo apt-get install nginx
```
## Installieren build-essential
```
sudo apt-get install build-essential
```
## Installieren von CMake
```
sudo apt-get install cmake
```
## Installieren von gdb
```
sudo apt-get install gdb
```

## Go Installieren in Ubuntu 22.10
Installation der offiziellen Distributionvon Go von der offiziellen Website
```
cd /tmp
wget https://go.dev/dl/go1.20.3.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.20.3.linux-amd64.tar.gz
```
### Einrichten von GOPATH:
```
mkdir ~/go
```
Öffnen Sie die ~/.bashrc-Datei in einem Texteditor:
```
nano ~/.bashrc
```
Fügen Sie die folgenden Zeilen am Ende der Datei hinzu:
```
export GOPATH=$HOME/go
export Rust=$HOME/.cargo/bin
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin:
```
Speichern Sie die Datei und schließen Sie den Editor.
```
source ~/.bashrc
```
### Go-Installation überprüfen
```
go version
```

## Rust Installieren in Ubuntu 22.10
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
### Rust Kongfiguration
```
source $HOME/.cargo/env
```
















