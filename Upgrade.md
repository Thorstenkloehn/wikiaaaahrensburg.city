## Was ist do-release-upgrade?
do-release-upgrade ist ein Befehl, der das Upgrade von Ubuntu durchführt. Es wird empfohlen, das Upgrade mit diesem Befehl durchzuführen, da es die meisten Probleme vermeidet.


## Wie führe ich ein Upgrade durch?
```
sudo apt-get update
sudo apt-get upgrade
do-release-upgrade
```

## Was ist der Unterschied zwischen einem Upgrade und einem Upgrade?
Ein Upgrade ist ein Upgrade von einer Version auf eine andere Version. Ein Upgrade ist ein Upgrade von einer Version auf eine andere Version.


