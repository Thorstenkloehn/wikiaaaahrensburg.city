## Erstellen von WebAssembly mit C++ in Windows 11 und CLion

### Voraussetzungen

Bevor Sie beginnen, stellen Sie sicher, dass Sie die folgenden Tools installiert haben:

* [CLion](https://www.jetbrains.com/clion/download/#section=windows)
* [Emscripten](https://emscripten.org/docs/getting_started/downloads.html)
* [Node.js](https://nodejs.org/en/download/)
* [Git bash](https://git-scm.com/downloads)

## Schritt 1: Emscripten installieren und einrichten in Windows 11


### Schritt 1.1: Emscripten herunterladen

* [Emscripten](https://emscripten.org/docs/getting_started/downloads.html) herunterladen und entpacken.
* git clone https://github.com/emscripten-core/emsdk.git e:\Emscripten
* Emscripten in das Verzeichnis e:\Emscripten kopieren.
* Nachdem Sie das Emscripten SDK heruntergeladen und installiert haben, führen Sie die folgenden Befehle aus, um das SDK zu aktivieren und Ihre Umgebung einzurichten (in Git Bash oder einer anderen Shell):

```
cd e:/Emscripten
emsdk install latest
emsdk activate latest
emsdk_env.bat



```

## Schritt 2: CLion konfigurieren

* CLion starten und ein neues Projekt erstellen.
* Navigieren Sie zu File > Settings > Build, Execution, Deployment > CMake.
* Fügen Sie im Feld "CMake options" die folgenden Optionen hinzu:
```
  -DCMAKE_TOOLCHAIN_FILE="e:/Emscripten/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake"
```

Klicken Sie auf "Apply" und dann auf "OK", um die Einstellungen zu speichern.       


## Schritt 3: C++-Quellcode schreiben  

* Erstellen Sie eine neue Datei mit dem Namen "hello.cpp" und fügen Sie den folgenden Code ein:

```
#include <emscripten.h>
#include <iostream>
    
int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
```

## Schritt 4: CMakeLists.txt erstellen

````

cmake_minimum_required(VERSION 3.10)
project(hello)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXECUTABLE_SUFFIX ".html")
add_executable(hello hello.cpp)
````



  

