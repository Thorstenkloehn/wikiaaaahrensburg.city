## Was ist certbot?
Certbot ist eine Software, die es Ihnen ermöglicht, SSL-Zertifikate zu erhalten und zu installieren. Es kann verwendet werden, um SSL-Zertifikate für verschiedene Domains zu erhalten.
## Wie installiere ich certbot Software bei Ubuntu ?
* Öffnen Sie ein Terminal und führen Sie den folgenden Befehl aus:
+ Aktualisieren Sie Ihre Paketliste und Pakete, indem Sie den folgenden Befehl ausführen:
```
sudo apt update && sudo apt upgrade
```
Installieren Sie Snapd mit dem folgenden Befehl:
```
sudo apt install snapd
```
Installieren Sie Certbot mit dem folgenden Befehl:
```
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot 
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo systemctl stop nginx
sudo certbot certonly --standalone -d ahrensburg.city -d www.ahrensburg.city
sudo systemctl start nginx
```

















