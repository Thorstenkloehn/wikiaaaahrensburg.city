## Um Nginx vollständig zu entfernen und dann auf Ubuntu neu zu installieren, folgen Sie diesen Schritten:

* Entfernen Sie Nginx und Abhängigkeiten:
```
sudo apt-get remove --purge nginx nginx-common
sudo apt-get autoremove
```
* Löschen Sie alle Nginx-Dateien:
```
sudo rm -rf /etc/nginx
```
* Installieren Sie Nginx:

```
sudo apt-get install nginx
```
+ nginx starten:

```
sudo systemctl start nginx
``` 

## Nginx Nutzer ändern:

```
sudo nano /etc/nginx/nginx.conf

```
### Ändern Sie den Nutzer auf den gewünschten Nutzer:

```
user thorsten;
```

## Nginx Konfiguration testen:

```
sudo nginx -t
```

## Nginx Konfiguration neu starten:

```
sudo nginx -s restart
```







