## Postgreq Installieren in Windows
### Schritt 1: Postgres herunterladen
* [Postgres](https://www.postgresql.org/download/windows/) herunterladen und installieren.
* Nachdem Sie das Postgres heruntergeladen und installiert haben, führen Sie die folgenden Befehle aus, um das SDK zu aktivieren und Ihre Umgebung einzurichten (in Git Bash oder einer anderen Shell):
```
cd e:/Postgres
```
### Schritt 2: Postgres konfigurieren
umgebungsvariable setzen
```
setx PATH "%PATH%;e:\PostgreSQL\14\bin"
```
#### Datenbank erstellen
##### Konsole öffnen
* psql -U postgres -h localhost -p 5432
* CREATE DATABASE openstreetmap;
* \c openstreetmap
* CREATE EXTENSION postgis;
* CREATE EXTENSION hstore;
* \q
##### openstrettmap daten hinunterladen
```
cd d:\data 
wget https://download.geofabrik.de/europe/germany/schleswig-holstein-latest.osm.pbf
```
#####  osm2pgsql herunterladen in Windows

* [hinterladen](https://osm2pgsql.org/download/windows/) und entpacken.
##### osm2pgsql umgebungsvariable setzen
```
setx PATH "%PATH%;E:\osm2pgsql-1.8.1-x64\osm2pgsql-bin"
```

##### osm2pgsql ausführen
```
osm2pgsql -c -d openstreetmap -U postgres -W -H localhost -P 5432 -S "E:\osm2pgsql-1.8.1-x64\osm2pgsql-bin\default.style" schleswig-holstein-latest.osm.pbf
```




